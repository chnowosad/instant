# Parking API

## Documentation

La documentation est disponible avec swagger via l'url suivante : http://localhost:8080/instant/apidoc

![alternativetext](readme/swagger.PNG)

## Compilation

Lancer la commande maven suivante à la racine du projet : *mvn clean install*

## Exécution

Démarrer le serveur comme une application java à partir de la classe *ParkingApplication*

Avec le profil *local*, la base de donnée est remplie au démarrage du serveur avec les paramètres pour les villes suivantes : 
- Bordeaux
- Rennes

## Login / Sécurité

A l'ouverture de la documentation swagger, un login est demandé.

3 utilisateurs ont été configurés : 

- user/user : Ne peut utiliser que le service de récupération des places disponibles (api/v1/parkings)
- read/read : Peut utiliser le service de récupération des places disponibles et dans la partie administration, lire les configurations et ajouter de nouvelles configurations
- admin/admin : Cet utilisateur a tous les droits

## Administration

L'API contient une partie administration, qui, en fonction des droits de l'utilisateur, peut lire/ajouter/modifier/supprimer la configuration d'un ou de plusieurs parkings

Note : 
Le service qui renvoie la liste des configurations est paginé.
En ce qui concerne le tri, les seules valeurs possibles sont city,asc et city,desc (tri ascendant et descendant en fonction de la ville, par défaut tri ascendant).

## Gestion des logs

2 appenders ont été créé : 
 - Console appender pour l'environnement local
 - Logstash appender pour les environnements de recette et de production (format natif (json) pour lire les logs dans Kibana (ELK))

Notes : 
 - Des données au format json sont ajoutées dans les logs afin de monitorer les temps de réponse des services (construction de dashboard Kibana)
 - Classe/Méthode/Ligne sont ajoutées dans les logs

## Gestion des erreurs

Les erreurs sont gérées avec le *ControllerAdvice* : 
 - Validation des paramètres (manquant, valeur)
 - Validation des données renvoyées (gestion des exceptions)

## Qualité

 - Sonarcube (sonarlint dans IntelliJ) est utilisé pour valider la qualité du code https://www.sonarlint.org/intellij/
 - JUnit : couverture du code : 
 
 ![alternativetext](readme/coverage.PNG)

## Base de donnée

Les données pour le parsing des services sont stockées en base dans la table **parking**.

Le service permet d'extraire des données au format XML comme pour les parkings de la ville de Bordeaux ou JSON comme pour Rennes.

Les paramètres en base sont les clés dans les fichiers XML ou JSON pour récupérer les données.

| city | url | type | path | name | total | free | pos |
| ------------- |:----------------------------------------------------------------------------------------------------------------------: | :-------------: | :-------------: | :-------------: | :-------------: | :-------------: | :-------------: | 
| bordeaux | http://data.lacub.fr/wfs?key=9Y2RU3FTE8&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=ST_PARK_P&SRSNAME=EPSG:4326 | xml | gml:featureMember | bm:NOM | bm:TOTAL | bm:LIBRES | gml:pos |
| rennes | https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=export-api-parking-citedia | json | records | key | max | free | geo |


En démarrant avec le profil *local*, les données sont visibles directement en base via l'url suivante : http://localhost:8080/instant/h2-console

![alternativetext](readme/database-connect.PNG)

![alternativetext](readme/database-select.PNG)

Une configuration prod/recette est nécessaire dans les fichiers propriétés associés.

## DevOps

Pour un déploiement sur les différents environnements, des configurations sont nécessaires (Docker / Jenkins / GitLab / Bamboo, ...)
