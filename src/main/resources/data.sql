DROP TABLE IF EXISTS parking;
CREATE TABLE parking (
  city varchar(50) NOT NULL PRIMARY KEY,
  url VARCHAR(250) NOT NULL,
  type VARCHAR(5) NOT NULL,
  path VARCHAR(50) DEFAULT NULL,
  name VARCHAR(50) DEFAULT NULL,
  total VARCHAR(50) DEFAULT NULL,
  free VARCHAR(50) DEFAULT NULL,
  pos VARCHAR(50) DEFAULT NULL
);
INSERT INTO parking (city, url, type, path, name, total, free, pos) VALUES ('bordeaux', 'http://data.lacub.fr/wfs?key=9Y2RU3FTE8&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=ST_PARK_P&SRSNAME=EPSG:4326', 'xml', 'gml:featureMember', 'bm:NOM', 'bm:TOTAL', 'bm:LIBRES', 'gml:pos');
INSERT INTO parking (city, url, type, path, name, total, free, pos) VALUES ('rennes', 'https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=export-api-parking-citedia', 'json', 'records', 'key', 'max', 'free', 'geo');
