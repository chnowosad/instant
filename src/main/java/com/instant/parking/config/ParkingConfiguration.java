package com.instant.parking.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;


@OpenAPIDefinition(
        info = @Info(
                title = "API parkings",
                version = "1.0",
                description = "API de gestion de parkings"
        )
)
@Configuration
public class ParkingConfiguration implements WebMvcConfigurer {

    @Autowired
    Environment environment;


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // Manage allowed clients url, to be implemented for rct and prod environments
        if (isLocal()) {
            registry.addMapping("/**").allowedOrigins("*");
        }
    }

    private boolean isLocal() {
        return Arrays.asList(environment.getActiveProfiles()).contains("local");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
                // Add session id in all log messages
                MDC.put("sessionId", httpServletRequest.getSession().getId());
                return true;
            }
            @Override
            public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {
                // Empty
            }
            @Override
            public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
                // Empty
            }
        });
    }

}
