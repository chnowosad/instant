package com.instant.parking.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TypeValidator.class)
public @interface ValidType {

    String message() default "{ValidType.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
