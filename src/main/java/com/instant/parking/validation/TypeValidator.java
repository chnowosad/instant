package com.instant.parking.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Type of configuration validation (xml or json)
 */
public class TypeValidator implements ConstraintValidator<ValidType, String> {

    @Override
    public boolean isValid(String type, ConstraintValidatorContext constraintValidatorContext) {
        return "xml".equals(type) || "json".equals(type);
    }

}
