package com.instant.parking.controller;

import com.instant.parking.dto.ParkingDto;
import com.instant.parking.dto.ErrorDto;
import com.instant.parking.exception.ParkingException;
import com.instant.parking.service.ParkingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

@Tag(description = "Controlleur gérant les parkings", name = "PARKING")
@RestController
@Validated
@RequestMapping("api/v1")
class ParkingController extends AbstractController {

    @Autowired
    private ParkingService parkingService;

    @Operation(description = "Liste des parkings")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des parkings renvoyée avec succès", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ParkingDto.class)))),
            @ApiResponse(responseCode = "404", description = "Aucune donnée disponible", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "400", description = "Données en entrée non valide", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @GetMapping(value = "/parkings", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<ParkingDto> getParkings(@NotEmpty @Parameter(description = "Ville") @Valid @RequestParam(value = "city") String city,
                                        @DecimalMin("-90") @DecimalMax("90") @Parameter(description = "Latitude") @Valid @RequestParam(value = "lat", required = true) BigDecimal lat,
                                        @DecimalMin("-180") @DecimalMax("180") @Parameter(description = "Longitude") @Valid @RequestParam(value = "lon", required = true) BigDecimal lon) throws ParkingException {
        long start = System.currentTimeMillis();
        List<ParkingDto> list = parkingService.getList(city, lat, lon);
        addLog("LISTE_PARKING", "Liste des parkings", start);
        return list;
    }

}
