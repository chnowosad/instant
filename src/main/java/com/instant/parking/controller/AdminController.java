package com.instant.parking.controller;

import com.instant.parking.dto.ErrorDto;
import com.instant.parking.dto.ParkingMappingDto;
import com.instant.parking.exception.ParkingMappingException;
import com.instant.parking.service.ParkingMappingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Tag(description = "Controlleur gérant l'administration des parkings en base", name = "ADMIN")
@RestController
@Validated
@RequestMapping("api/v1/admin")
public class AdminController {

    @Autowired
    private ParkingMappingService service;

    @Operation(description = "Liste des parkings avec configuration")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des parkings renvoyée avec succès", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ParkingMappingDto.class)))),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @GetMapping("/get")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_READ')")
    @ResponseStatus(HttpStatus.OK)
    public Page<ParkingMappingDto> getAll(@ParameterObject @PageableDefault(size = 5) Pageable pageable) {
        return service.getAll(pageable);
    }

    @Operation(description = "Configuration du parking donné")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Configuration parking renvoyée avec succès", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ParkingMappingDto.class)))),
            @ApiResponse(responseCode = "400", description = "Donnée en entrée non valide", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "404", description = "Le parking n'est pas configuré", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @GetMapping("/get/{city}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_READ')")
    @ResponseStatus(HttpStatus.OK)
    public ParkingMappingDto get(@PathVariable @NotEmpty String city) throws ParkingMappingException {
        return service.get(city);
    }

    @Operation(description = "Ajout configuration d'un parking")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Configuration parking ajoutée avec succès"),
            @ApiResponse(responseCode = "400", description = "Le parking existe déjà ou donnée invalide", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @PostMapping("add")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMIN_READ')")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@Valid @RequestBody ParkingMappingDto park) throws ParkingMappingException {
        service.save(park);
    }

    @Operation(description = "Mise à jour configuration d'un parking donné")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Mise à jour parking faite avec succès"),
            @ApiResponse(responseCode = "400", description = "Donnée invalide", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "404", description = "Le parking n'existe pas", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @PutMapping("update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@Valid @RequestBody ParkingMappingDto park) throws ParkingMappingException {
        service.update(park);
    }

    @Operation(description = "Suppression des configuration parking")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Suppression faite avec succès"),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @DeleteMapping("delete-all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAll() {
        service.deleteAll();
    }

    @Operation(description = "Suppression configuration parking donné")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Suppression faite avec succès"),
            @ApiResponse(responseCode = "400", description = "Données non valide", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "404", description = "Le parking n'existe pas", content = @Content(schema = @Schema(implementation = ErrorDto.class))),
            @ApiResponse(responseCode = "500", description = "Erreur inattendue", content = @Content(schema = @Schema(implementation = ErrorDto.class)))
    })
    @DeleteMapping("/delete/{city}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable @NotEmpty String city) throws ParkingMappingException {
        service.delete(city);
    }

}
