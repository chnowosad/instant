package com.instant.parking.controller;

import lombok.extern.log4j.Log4j2;

import static net.logstash.logback.argument.StructuredArguments.v;

@Log4j2
abstract class AbstractController {

    /**
     * Add a log message with the json format for the service response time monitoring
     *
     * @param key         log key
     * @param description log description
     * @param startTime   start time
     */
    protected void addLog(String key, String description, long startTime) {
        log.info("Monitoring service {} description {} duration {}",
                v("serviceKey", key), v("serviceDescription", description), v("duration", System.currentTimeMillis() - startTime));
    }

}
