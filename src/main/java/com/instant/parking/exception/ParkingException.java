package com.instant.parking.exception;

/**
 * Parking specific exception
 */
public class ParkingException extends Exception {

    /**
     * Default constructor
     * @param message error message
     */
    public ParkingException(String message) {
        super(message);
    }

}
