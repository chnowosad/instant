package com.instant.parking.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Specific parking mapping exception
 */
public class ParkingMappingException extends Exception {

    @Getter
    private final HttpStatus status;

    /**
     * Default constructor
     * @param message message
     * @param status http status
     */
    public ParkingMappingException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

}
