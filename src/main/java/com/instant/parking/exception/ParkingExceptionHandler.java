package com.instant.parking.exception;

import com.instant.parking.dto.ErrorDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.springframework.http.HttpStatus.*;

@Log4j2
@ControllerAdvice
public class ParkingExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> globalExceptionHandler(Exception ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "Erreur inattendue", INTERNAL_SERVER_ERROR.toString());
        return new ResponseEntity<>(responseError, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorDto> accessDeniedExceptionHandler(AccessDeniedException ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "L'utilisateur n'a pas les droits suffisants pour cette action", UNAUTHORIZED.toString());
        return new ResponseEntity<>(responseError, UNAUTHORIZED);
    }

    @ExceptionHandler(ParkingException.class)
    public ResponseEntity<ErrorDto> parkingExceptionHandler(Exception ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "Aucune donnée disponible", NOT_FOUND.toString());
        return new ResponseEntity<>(responseError, NOT_FOUND);
    }

    @ExceptionHandler(ParkingMappingException.class)
    public ResponseEntity<ErrorDto> parkingMappingExceptionHandler(ParkingMappingException ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), ex.getMessage(), ex.getStatus().toString());
        return new ResponseEntity<>(responseError, ex.getStatus());
    }

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolationExceptionException(ConstraintViolationException ex, WebRequest request) {
        log.error("Parametre(s) non valide", ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "Paramètre(s) non valide", BAD_REQUEST.toString());
        if (ex.getConstraintViolations() != null) {
            ex.getConstraintViolations().forEach(e -> responseError.getErrors().add((e.getPropertyPath() + " " + e.getMessage())));
        }
        return new ResponseEntity<>(responseError, BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String name = ex.getParameterName();
        log.error("Parametre manquant {}", name);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "Le paramètre obligatoire " + name + " est manquant", BAD_REQUEST.toString());
        return new ResponseEntity<>(responseError, BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorDto responseError = new ErrorDto(LocalDateTime.now(ZoneOffset.UTC), "Paramètre(s) non valide", BAD_REQUEST.toString());
        ex.getBindingResult().getFieldErrors().forEach(e -> responseError.getErrors().add(e.getDefaultMessage() + " - " + e.getRejectedValue()));
        ex.getBindingResult().getGlobalErrors().forEach(e -> responseError.getErrors().add(e.getObjectName() + " " + e.getDefaultMessage()));
        return new ResponseEntity<>(responseError, BAD_REQUEST);
    }

}
