package com.instant.parking.dao;

import com.instant.parking.entity.ParkingEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Parking DAO
 */
public interface ParkingDao extends PagingAndSortingRepository<ParkingEntity, Long> {

    /**
     * Exist parking by city
     * @param city city
     * @return true or false
     */
    boolean existsByCity(String city);

    /**
     * Find parking configuration by city
     * @param city city
     * @return parking configuration
     */
    ParkingEntity findByCity(String city);

    /**
     * Delete parking configuration by city
     * @param city city
     */
    void deleteByCity(String city);

}
