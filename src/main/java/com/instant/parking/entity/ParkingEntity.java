package com.instant.parking.entity;

import com.googlecode.jmapper.annotations.JMap;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "parking", schema = "public")
public class ParkingEntity implements Serializable {

    @JMap
    @Id
    private String city;

    @JMap
    private String url;
    @JMap
    private String type;
    @JMap
    private String path;
    @JMap
    private String name;
    @JMap
    private String total;
    @JMap
    private String free;
    @JMap
    private String pos;

}
