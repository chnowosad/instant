package com.instant.parking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Schema(description = "Erreur")
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto implements Serializable {

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Getter
    private LocalDateTime timestamp;

    @Schema(description = "Status de la réponse", example = "400 BAD_REQUEST")
    @Getter
    private String status;

    @Schema(description = "Message d'erreur", example = "Le paramètre obligatoire city est manquant")
    @Getter
    private String message;

    @JsonInclude(NON_EMPTY)
    @Schema(description = "Liste des erreurs", hidden = true)
    private List<String> errors;

    public ErrorDto(LocalDateTime timestamp, String message, String status) {
        this.timestamp = timestamp;
        this.message = message;
        this.status = status;
    }

    public List<String> getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        return this.errors;
    }

}
