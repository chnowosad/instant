package com.instant.parking.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionDto implements Serializable {

    @Schema(description = "Longitude", example = "-0.701561")
    private Double lon;
    @Schema(description = "Latitude", example = "44.774274")
    private Double lat;

}
