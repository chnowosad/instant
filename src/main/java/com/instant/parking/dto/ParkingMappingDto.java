package com.instant.parking.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.instant.parking.validation.ValidType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Schema(description = "Parking Mapping")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingMappingDto implements Serializable {

    @JMap
    @Schema(description = "Ville", example = "bordeaux")
    @NotEmpty(message = "La ville est obligatoire")
    private String city;
    @JMap
    @Schema(description = "URL API liste des données", example = "http://myurl")
    @NotEmpty(message = "L'url est obligatoire")
    private String url;
    @JMap
    @Schema(description = "Type de donnée xml ou json", example = "xml")
    @ValidType(message = "Le type n'est pas valide, valeurs accepées : xml ou json")
    private String type;
    @JMap
    @Schema(description = "Chemin accès pour mapping liste des parkings", example = "gml:featureMember")
    @NotEmpty(message = "Le path est obligatoire")
    private String path;
    @JMap
    @Schema(description = "Mapping nom du parking", example = "bm:NOM")
    @NotEmpty(message = "Le nom est obligatoire")
    private String name;
    @JMap
    @Schema(description = "Mapping nombre total de places dans le parking", example = "bm:TOTAL")
    @NotEmpty(message = "Le total est obligatoire")
    private String total;
    @JMap
    @Schema(description = "Mapping nombre de places libre dans le parking", example = "bm:LIBRES")
    @NotEmpty(message = "Le champ free est obligatoire")
    private String free;
    @JMap
    @Schema(description = "Mapping geolocalisation du parking", example = "gml:pos")
    @NotEmpty(message = "Le champ pos est obligatoire")
    private String pos;

}
