package com.instant.parking.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Schema(description = "Parking")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingDto implements Serializable {

    @Schema(description = "Nom du parking", example = "Parking de la gare")
    private String name;
    @Schema(description = "Nombre maximum de places dans le parking", example = "127")
    private Integer max;
    @Schema(description = "Nombre de places disponibles dans le parking", example = "12")
    private Integer free;
    @Schema(description = "Distance entre le position donnée et le parking en mètre", example = "350")
    private Integer distance;
    @Schema(description = "Localisation du parking")
    private PositionDto position;

}
