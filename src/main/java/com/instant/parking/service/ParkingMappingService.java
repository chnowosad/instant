package com.instant.parking.service;

import com.instant.parking.dto.ParkingMappingDto;
import com.instant.parking.exception.ParkingMappingException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Parking configuration service
 */
public interface ParkingMappingService {

    /**
     * Retrieve all configuration
     * @param pageable pagination and sorting parameter
     * @return parking configuration list
     */
    Page<ParkingMappingDto> getAll(Pageable pageable);

    /**
     * Retrieve given city parking configuration
     * @param city city
     * @return parking configuration
     * @throws ParkingMappingException specific exception
     */
    ParkingMappingDto get(String city) throws ParkingMappingException;

    /**
     * Create a parking configuration
     * @param park parking configuration
     * @throws ParkingMappingException specific exception
     */
    void save(ParkingMappingDto park) throws ParkingMappingException;

    /**
     * Update a parking configuration
     * @param park parking configuration
     * @throws ParkingMappingException specific exception
     */
    void update(ParkingMappingDto park) throws ParkingMappingException;

    /**
     * Delete all
     */
    void deleteAll();

    /**
     * Delete given city parking configuration
     * @param city city
     * @throws ParkingMappingException specific exception
     */
    void delete(String city) throws ParkingMappingException;
}
