package com.instant.parking.service;

import com.instant.parking.dto.ParkingDto;
import com.instant.parking.exception.ParkingException;

import java.math.BigDecimal;
import java.util.List;

/**
 * Service used to retrieve data on the car park
 */
public interface ParkingService {

    /**
     * Retrieve the given city car parks data
     * @param city city
     * @param lat lattitude
     * @param lon lngitude
     * @return data on the city parks
     * @throws ParkingException
     */
    List<ParkingDto> getList(String city,BigDecimal lat, BigDecimal lon) throws ParkingException;

}
