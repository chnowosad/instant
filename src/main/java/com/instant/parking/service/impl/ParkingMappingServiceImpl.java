package com.instant.parking.service.impl;

import com.googlecode.jmapper.JMapper;
import com.instant.parking.dao.ParkingDao;
import com.instant.parking.dto.ParkingMappingDto;
import com.instant.parking.entity.ParkingEntity;
import com.instant.parking.exception.ParkingMappingException;
import com.instant.parking.service.ParkingMappingService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Transactional
@Service
public class ParkingMappingServiceImpl implements ParkingMappingService {

    private final JMapper<ParkingMappingDto, ParkingEntity> toDto = new JMapper<>(ParkingMappingDto.class, ParkingEntity.class);
    private final JMapper<ParkingEntity, ParkingMappingDto> toEntity = new JMapper<>(ParkingEntity.class, ParkingMappingDto.class);

    @Autowired
    private ParkingDao parkingDao;

    private static final String CITY_ASC = "city: ASC";
    private static final String CITY_DESC = "city: DESC";

    @Override
    public Page<ParkingMappingDto> getAll(Pageable pageable) {
        List<ParkingMappingDto> retList = new ArrayList<>();
        String sort = pageable.getSort().toString();
        Pageable pageTmp = pageable;
        if (!CITY_ASC.equals(sort) && !CITY_DESC.equals(sort)) {
            pageTmp = PageRequest.of(pageable.getPageNumber(), pageTmp.getPageSize(), Sort.by("city"));
        }
        Page<ParkingEntity> page = parkingDao.findAll(pageTmp);
        page.get().forEach(e -> retList.add(toDto.getDestination(e)));
        return new PageImpl<>(retList, pageable, page.getTotalElements());
    }

    @Override
    public ParkingMappingDto get(String city) throws ParkingMappingException {
        if (!parkingDao.existsByCity(city)) {
            throw new ParkingMappingException("Le parking " + city + " n'existe pas", HttpStatus.NOT_FOUND);
        }
        return toDto.getDestination(parkingDao.findByCity(city));
    }

    @Override
    public void save(ParkingMappingDto park) throws ParkingMappingException {
        if (parkingDao.existsByCity(park.getCity())) {
            throw new ParkingMappingException("Le parking " + park.getCity() + " existe déjà", HttpStatus.BAD_REQUEST);
        }
        parkingDao.save(toEntity.getDestination(park));
    }

    @Override
    public void update(ParkingMappingDto park) throws ParkingMappingException {
        if (!parkingDao.existsByCity(park.getCity())) {
            throw new ParkingMappingException("Le parking " + park.getCity() + " n'existe pas", HttpStatus.NOT_FOUND);
        }
        ParkingEntity entity = parkingDao.findByCity(park.getCity());
        entity.setName(park.getName());
        entity.setPos(park.getPos());
        entity.setFree(park.getFree());
        entity.setTotal(park.getTotal());
        entity.setType(park.getType());
        entity.setUrl(park.getUrl());
        entity.setPath(park.getPath());
        parkingDao.save(entity);
    }

    @Override
    public void deleteAll() {
        parkingDao.deleteAll();
    }

    @Override
    public void delete(String city) throws ParkingMappingException {
        if (!parkingDao.existsByCity(city)) {
            throw new ParkingMappingException("Le parking " + city + " n'existe pas", HttpStatus.NOT_FOUND);
        }
        parkingDao.deleteByCity(city);
    }

}
