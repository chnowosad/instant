package com.instant.parking.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.instant.parking.dao.ParkingDao;
import com.instant.parking.dto.ParkingDto;
import com.instant.parking.dto.PositionDto;
import com.instant.parking.entity.ParkingEntity;
import com.instant.parking.exception.ParkingException;
import com.instant.parking.service.DistanceService;
import com.instant.parking.service.ParkingService;
import lombok.extern.log4j.Log4j2;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Transactional
@Service
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private DistanceService distanceService;
    @Autowired
    private ParkingDao parkingDao;
    @Autowired
    private CustomRestTemplate restTemplate;

    @Override
    public List<ParkingDto> getList(String city, BigDecimal lat, BigDecimal lon) throws ParkingException {
        List<ParkingDto> list = new ArrayList<>();
        ParkingEntity parkingEntity = parkingDao.findByCity(city);
        if (parkingEntity != null) {
            JsonNode node = getJsonNode(parkingEntity.getUrl(), parkingEntity.getType());
            if (node != null && node.findValue(parkingEntity.getPath()) != null) {
                node.findValue(parkingEntity.getPath()).forEach(e -> {
                    PositionDto position = getPosition(parkingEntity, e);
                    int dist = distanceService.get(lat.doubleValue(), lon.doubleValue(), position.getLat(), position.getLon());
                    ParkingDto park = new ParkingDto(getString(e, parkingEntity.getName()), getInt(e, parkingEntity.getTotal()), getInt(e, parkingEntity.getFree()), dist, position);
                    list.add(park);
                });
            } else {
                throw new ParkingException("Parking list not found");
            }
        } else {
            throw new ParkingException("Parking not found");
        }
        return list;
    }

    private String getString(JsonNode node, String key) {
        JsonNode tmp = node.findValue(key);
        if (tmp != null) {
            return tmp.asText();
        } else {
            log.warn("key does not exist {}", key);
            return "";
        }
    }

    private int getInt(JsonNode node, String key) {
        JsonNode tmp = node.findValue(key);
        if (tmp != null) {
            return tmp.asInt();
        } else {
            log.warn("key does not exist {}", key);
            return 0;
        }
    }

    private PositionDto getPosition(ParkingEntity parkingEntity, JsonNode node) {
        double latPark = 0;
        double lonPark = 0;
        JsonNode tmp = node.findValue(parkingEntity.getPos());
        if (tmp != null) {
            if (tmp.isArray() && tmp.size() == 2) {
                latPark = tmp.get(0).asDouble();
                lonPark = tmp.get(1).asDouble();
            } else {
                String[] pos = tmp.asText().split(" ");
                if (pos.length == 2) {
                    latPark = Double.parseDouble(pos[0]);
                    lonPark = Double.parseDouble(pos[1]);
                }
            }
        }
        return new PositionDto(lonPark, latPark);
    }

    private JsonNode getJsonNode(String url, String type) throws ParkingException {
        JsonNode node = null;
        try {
            if ("xml".equals(type)) {
                String xml = restTemplate.getForObject(url, String.class);
                if (xml != null) {
                    String json = XML.toJSONObject(xml).toString();
                    node = new ObjectMapper().readTree(json);
                }
            } else {
                node = restTemplate.getForObject(url, JsonNode.class);
            }
        } catch (Exception e) {
            log.error("An error occurred while retrieving the json node", e);
            throw new ParkingException("An error occurred while retrieving the json node");
        }
        return node;
    }

}
