package com.instant.parking.service;

/**
 * Distance service
 */
public interface DistanceService {

    /**
     * Retrieve the distance in meter between 2 GPS points
     * @param lat1 latitude point 1
     * @param lon1 longitude point 1
     * @param lat2 latitude point 2
     * @param lon2 longitude point 2
     * @return distance in meter between the 2 points
     */
    int get(double lat1, double lon1, double lat2, double lon2);

}
