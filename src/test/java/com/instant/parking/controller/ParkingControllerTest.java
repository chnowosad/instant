package com.instant.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.instant.parking.dto.ErrorDto;
import com.instant.parking.dto.ParkingDto;
import com.instant.parking.dto.PositionDto;
import com.instant.parking.service.ParkingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ParkingControllerTest {

    private static final String URL = "/api/v1/parkings";
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ParkingService parkingService;

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetParkingsMissingParamCity() throws Exception {
        MvcResult mvcResult = this.mvc.perform(get(URL).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("Le paramètre obligatoire city est manquant", error.getMessage());
        Assert.assertEquals("400 BAD_REQUEST", error.getStatus());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetParkingsMissingParamLat() throws Exception {
        RequestBuilder requestBuilder = get(URL).contentType(MediaType.APPLICATION_JSON).param("city", "bordeaux");
        MvcResult mvcResult = this.mvc.perform(requestBuilder).andExpect(status().isBadRequest()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("Le paramètre obligatoire lat est manquant", error.getMessage());
        Assert.assertEquals("400 BAD_REQUEST", error.getStatus());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetParkingsMissingParamLon() throws Exception {
        RequestBuilder requestBuilder = get(URL).contentType(MediaType.APPLICATION_JSON).param("city", "bordeaux").param("lat", "0");
        MvcResult mvcResult = this.mvc.perform(requestBuilder).andExpect(status().isBadRequest()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("Le paramètre obligatoire lon est manquant", error.getMessage());
        Assert.assertEquals("400 BAD_REQUEST", error.getStatus());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetParkingsInvalidParam() throws Exception {
        RequestBuilder requestBuilder = get(URL).contentType(MediaType.APPLICATION_JSON)
                .param("city", "")
                .param("lat", "91")
                .param("lon", "-181");
        MvcResult mvcResult = this.mvc.perform(requestBuilder).andExpect(status().isBadRequest()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("Paramètre(s) non valide", error.getMessage());
        Assert.assertEquals("400 BAD_REQUEST", error.getStatus());
        Assert.assertEquals(3, error.getErrors().size());
        Assert.assertTrue(error.getErrors().contains("getParkings.lat must be less than or equal to 90"));
        Assert.assertTrue(error.getErrors().contains("getParkings.lon must be greater than or equal to -180"));
        Assert.assertTrue(error.getErrors().contains("getParkings.city must not be empty"));
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetParkingsSuccess() throws Exception {
        given(parkingService.getList(any(), any(), any())).willReturn(getParkingList());
        RequestBuilder requestBuilder = get(URL).contentType(MediaType.APPLICATION_JSON)
                .param("city", "bordeaux")
                .param("lat", "0")
                .param("lon", "0");
        MvcResult mvcResult = this.mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ParkingDto[] res = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ParkingDto[].class);
        Assert.assertNotNull(res);
        Assert.assertEquals(1, res.length);
        Assert.assertEquals("name", res[0].getName());
    }

    private List<ParkingDto> getParkingList() {
        List<ParkingDto> list = new ArrayList<>();
        ParkingDto park = new ParkingDto("name", 100, 10, 50, new PositionDto(0.0, 0.0));
        list.add(park);
        return list;
    }

}
