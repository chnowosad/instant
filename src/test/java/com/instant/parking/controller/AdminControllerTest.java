package com.instant.parking.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.instant.parking.dto.ErrorDto;
import com.instant.parking.dto.ParkingMappingDto;
import com.instant.parking.service.ParkingMappingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class AdminControllerTest {

    private static final String URL = "/api/v1/admin/";
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ParkingMappingService service;

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testGetAllNoRight() throws Exception {
        MvcResult mvcResult = this.mvc.perform(get(URL + "get").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "read", password = "read", roles = "ADMIN_READ")
    @Test
    public void testGetAll() throws Exception {
        this.mvc.perform(get(URL + "get").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testDeleteAllNoRightUser() throws Exception {
        MvcResult mvcResult = this.mvc.perform(delete(URL + "delete-all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "read", password = "read", roles = "ADMIN_READ")
    @Test
    public void testDeleteAllNoRightRead() throws Exception {
        MvcResult mvcResult = this.mvc.perform(delete(URL + "delete-all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    @Test
    public void testDeleteAll() throws Exception {
        this.mvc.perform(delete(URL + "delete-all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testDeleteNoRightUser() throws Exception {
        MvcResult mvcResult = this.mvc.perform(delete(URL + "delete/bordeaux").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "read", password = "read", roles = "ADMIN_READ")
    @Test
    public void testDeleteNoRightRead() throws Exception {
        MvcResult mvcResult = this.mvc.perform(delete(URL + "delete/bordeaux").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    @Test
    public void testDelete() throws Exception {
        this.mvc.perform(delete(URL + "delete/bordeaux").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
    }

    @WithMockUser(username = "user", password = "user", roles = "USER")
    @Test
    public void testUpdateNoRightUser() throws Exception {
        MvcResult mvcResult = this.mvc.perform(put(URL + "update").contentType(MediaType.APPLICATION_JSON).content(getParkJson()))
                .andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "read", password = "read", roles = "ADMIN_READ")
    @Test
    public void testUpdateNoRightRead() throws Exception {
        MvcResult mvcResult = this.mvc.perform(put(URL + "update").contentType(MediaType.APPLICATION_JSON).content(getParkJson()))
                .andExpect(status().isUnauthorized()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        ErrorDto error = mapper.readValue(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8), ErrorDto.class);
        Assert.assertNotNull(error);
        Assert.assertEquals("L'utilisateur n'a pas les droits suffisants pour cette action", error.getMessage());
        Assert.assertEquals("401 UNAUTHORIZED", error.getStatus());
    }

    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    @Test
    public void testUpdate() throws Exception {
        this.mvc.perform(put(URL + "update").contentType(MediaType.APPLICATION_JSON).content(getParkJson())).andExpect(status().isNoContent());
    }

    private String getParkJson() throws JsonProcessingException {
        ParkingMappingDto park = new ParkingMappingDto();
        park.setCity("bordeaux");
        park.setFree("free");
        park.setName("name");
        park.setPath("path");
        park.setPos("pos");
        park.setTotal("tot");
        park.setType("json");
        park.setUrl("url");
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(park);
    }

}
