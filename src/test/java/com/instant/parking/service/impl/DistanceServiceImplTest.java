package com.instant.parking.service.impl;

import com.instant.parking.service.DistanceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DistanceServiceImplTest {

    @Autowired
    private DistanceService service;

    @Test
    public void testGet() {
        int dist = service.get(0, 0, 0, 0);
        Assert.assertEquals(0, dist);
        dist = service.get(44.888824, -0.517676, 44.797293, -0.553247);
        Assert.assertEquals(10556, dist);
    }

}
