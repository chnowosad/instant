package com.instant.parking.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.instant.parking.dao.ParkingDao;
import com.instant.parking.dto.ParkingDto;
import com.instant.parking.entity.ParkingEntity;
import com.instant.parking.exception.ParkingException;
import com.instant.parking.service.DistanceService;
import com.instant.parking.service.ParkingService;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ParkingServiceImplTest {

    @Mock
    private ParkingDao parkingDao;
    @Mock
    private DistanceService distanceService;
    @Mock
    private CustomRestTemplate restTemplate;

    @InjectMocks
    private ParkingService service = new ParkingServiceImpl();

    @Test
    public void testGetListNotExist() {
        try {
            service.getList("", BigDecimal.valueOf(0), BigDecimal.valueOf(0));
        } catch (ParkingException e) {
            Assert.assertEquals("Parking not found", e.getMessage());
        }
    }

    @Test
    public void testGetListNotFound() throws Exception {
        try {
            when(parkingDao.findByCity("rennes")).thenReturn(getEntity("http://myurl", "rennes", "json", "", "", "", "", ""));
            when(restTemplate.getForObject("http://myurl", JsonNode.class)).thenReturn(getJsonNode("data-rennes.json"));
            service.getList("rennes", BigDecimal.valueOf(0), BigDecimal.valueOf(0));
        } catch (ParkingException e) {
            Assert.assertEquals("Parking list not found", e.getMessage());
        }
    }

    @Test
    public void testGetListWrong() throws Exception {
        when(parkingDao.findByCity("rennes")).thenReturn(getEntity("http://myurl", "rennes", "json", "", "records", "", "", ""));
        when(restTemplate.getForObject("http://myurl", JsonNode.class)).thenReturn(getJsonNode("data-rennes.json"));
        List<ParkingDto> list = service.getList("rennes", BigDecimal.valueOf(0), BigDecimal.valueOf(0));
        Assert.assertNotNull(list);
        Assert.assertEquals(10, list.size());
        Assert.assertEquals("", list.get(5).getName());
        Assert.assertEquals(Integer.valueOf(0), list.get(5).getMax());
        Assert.assertEquals(Integer.valueOf(0), list.get(5).getFree());
        Assert.assertEquals(Double.valueOf(0), list.get(5).getPosition().getLat());
        Assert.assertEquals(Double.valueOf(0), list.get(5).getPosition().getLon());
    }

    @Test
    public void testGetListJson() throws Exception {
        when(parkingDao.findByCity("rennes")).thenReturn(getEntity("http://myurl", "rennes", "json", "key", "records", "max", "free", "geo"));
        when(restTemplate.getForObject("http://myurl", JsonNode.class)).thenReturn(getJsonNode("data-rennes.json"));
        List<ParkingDto> list = service.getList("rennes", BigDecimal.valueOf(0), BigDecimal.valueOf(0));
        Assert.assertNotNull(list);
        Assert.assertEquals(10, list.size());
        Assert.assertEquals("Kennedy", list.get(5).getName());
        Assert.assertEquals(Integer.valueOf(193), list.get(5).getMax());
        Assert.assertEquals(Integer.valueOf(44), list.get(5).getFree());
        Assert.assertEquals(Double.valueOf(48.12071853), list.get(5).getPosition().getLat());
        Assert.assertEquals(Double.valueOf(-1.709662086), list.get(5).getPosition().getLon());
    }

    @Test
    public void testGetListXml() throws Exception {
        when(parkingDao.findByCity("bordeaux")).thenReturn(getEntity("http://myurl", "bordeaux", "xml", "bm:NOM", "gml:featureMember", "bm:TOTAL", "bm:LIBRES", "gml:pos"));
        when(restTemplate.getForObject("http://myurl", String.class)).thenReturn(getString("data-bordeaux.xml"));
        List<ParkingDto> list = service.getList("bordeaux", BigDecimal.valueOf(0), BigDecimal.valueOf(0));
        Assert.assertNotNull(list);
        Assert.assertEquals(85, list.size());
        Assert.assertEquals("Victoire", list.get(5).getName());
        Assert.assertEquals(Integer.valueOf(477), list.get(5).getMax());
        Assert.assertEquals(Integer.valueOf(212), list.get(5).getFree());
        Assert.assertEquals(Double.valueOf(44.831029), list.get(5).getPosition().getLat());
        Assert.assertEquals(Double.valueOf(-0.5719), list.get(5).getPosition().getLon());
    }

    private String getString(String name) throws IOException {
        File file = new ClassPathResource(name).getFile();
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    private JsonNode getJsonNode(String name) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File jsonFile = new ClassPathResource(name).getFile();
        return mapper.readTree(jsonFile);
    }

    private ParkingEntity getEntity(String url, String city, String type, String name, String path, String total, String free, String pos) {
        ParkingEntity entity = new ParkingEntity();
        entity.setUrl(url);
        entity.setType(type);
        entity.setName(name);
        entity.setPath(path);
        entity.setTotal(total);
        entity.setFree(free);
        entity.setCity(city);
        entity.setPos(pos);
        return entity;
    }

}
